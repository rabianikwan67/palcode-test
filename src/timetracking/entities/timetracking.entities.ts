import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { IsInt, IsNotEmpty, Min } from "class-validator"

@Entity()
export class TimeTracking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: "int" })
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  user_id: number;

  @Column({ type: "int" })
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  task_id: number;

  @Column({ type: "int" })
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  duration: number;
}