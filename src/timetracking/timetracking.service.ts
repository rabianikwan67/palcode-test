import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TimeTracking } from './entities';
import { CreateTimeLogDto } from './dto';

@Injectable()
export class TimetrackingService {
    constructor(
        @InjectRepository(TimeTracking)
        private timeTrackingRepo: Repository<TimeTracking>
    ) {}

    async create(payload: CreateTimeLogDto) {
        return await this.timeTrackingRepo.create(payload)
    }

    async findAll() {
        return this.timeTrackingRepo.find();
    }
}
