import { IsInt, IsNotEmpty, IsNumber } from "class-validator";

export class CreateTimeLogDto {
    @IsNotEmpty()
    @IsInt()
    @IsNumber()
    user_id: number

    @IsNotEmpty()
    @IsInt()
    @IsNumber()
    task_id: number

    @IsNotEmpty()
    @IsInt()
    @IsNumber()
    duration: number
}